<?php

namespace Ens\JobeetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\PrePersist;

/**
 * Affiliate
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Affiliate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255)
     */
    private $token;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var array
     * 
     * @ORM\ManyToMany(targetEntity="Category")
     * @ORM\JoinTable(
     * 		name="category_affiliate",
     * 		joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")},
     * 		inverseJoinColumns={@JoinColumn(
     * 								name="group_id",
     * 								referencedColumnName="id"
     * 							)}
     * )
     */
    private $category_affiliate;
    
    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
    	$this->createdAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Affiliate
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Affiliate
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Affiliate
     */
    public function setToken($token)
    {
        $this->token = $token;
    
        return $this;
    }

    /**
     * Get token
     *
     * @return string 
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Affiliate
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Affiliate
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category_affiliate = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add category_affiliate
     *
     * @param \Ens\JobeetBundle\Entity\Category $categoryAffiliate
     * @return Affiliate
     */
    public function addCategoryAffiliate(\Ens\JobeetBundle\Entity\Category $categoryAffiliate)
    {
        $this->category_affiliate[] = $categoryAffiliate;
    
        return $this;
    }

    /**
     * Remove category_affiliate
     *
     * @param \Ens\JobeetBundle\Entity\Category $categoryAffiliate
     */
    public function removeCategoryAffiliate(\Ens\JobeetBundle\Entity\Category $categoryAffiliate)
    {
        $this->category_affiliate->removeElement($categoryAffiliate);
    }

    /**
     * Get category_affiliate
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategoryAffiliate()
    {
        return $this->category_affiliate;
    }
}